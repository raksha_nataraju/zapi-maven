package com.thed.zapi.mojo;

/**
 * Created by Zephyr Dev on 1/31/14.
 */

import java.io.File;
import java.util.List;

import com.google.common.collect.ImmutableList;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;


/**
 * Update test results to Zephyr Server.
 *
 * @author <a href="mailto:developer@getzephyr.com">Zephyr Dev</a>
 */
@Mojo(name = "zapi", inheritByDefault = false, defaultPhase = LifecyclePhase.POST_INTEGRATION_TEST)
@Execute(phase = LifecyclePhase.POST_INTEGRATION_TEST)
public class ZapiReportMojo extends AbstractZapiReportMojo{

    /**
     * The filename to use for the report.
     *
     * @noinspection UnusedDeclaration
     */
    @Parameter(defaultValue = "surefire-report", property = "outputName", required = true)
    private String outputName;

    /**
     * If set to true the surefire report will be generated even when there are no surefire result files.
     * Defaulyts to <code>true</code> to preserve legacy behaviour pre 2.10.
     *
     * @noinspection UnusedDeclaration
     * @since 2.11
     */
    @Parameter(defaultValue = "true", property = "alwaysGenerateSurefireReport")
    private boolean alwaysGenerateSurefireReport;

    /**
     * If set to true the surefire report generation will be skipped.
     *
     * @noinspection UnusedDeclaration
     * @since 2.11
     */
    @Parameter(defaultValue = "false", property = "skipZapiReport")
    private boolean skipZapiReport;



    protected List<File> getSurefireReportsDirectory( MavenProject subProject ){
        String buildDir = subProject.getBuild().getDirectory();
        return ImmutableList.<File>of(new File( buildDir + "/surefire-reports" ), new File( buildDir + "/failsafe-reports" ));
    }

    public String getOutputName()
    {
        return outputName;
    }

    protected boolean isSkipped()
    {
        return skipZapiReport;
    }

    protected boolean isGeneratedWhenNoResults()
    {
        return alwaysGenerateSurefireReport;
    }

}